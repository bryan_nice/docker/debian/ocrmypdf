# ocrmypdf

## Example Usage

```bash
docker run \
  -it \
  --rm \
  -v ${PWD}:/home/ocrmypdf \
  bryannice/ocrmypdf \
    -l eng \
    --output-type pdf \
    <INPUT PDF>.pdf \
    <OUTPUT_PDF>.pdf
```
